const SitemapPlugin = require('sitemap-webpack-plugin')
const sitemapJSON = require('./public/sitemap.json')

const config = {
  base: process.env.VUE_APP_BASE_API_DOMAIN,
  paths: sitemapJSON.routes,
  options: { filename: 'sitemap.xml' }
}

module.exports = {
  // mode: 'production',
  css: {
    extract: { ignoreOrder: true },
    loaderOptions: {
      scss: {
        prependData: `
          @import "./src/assets/styles/variables.scss";
        `
      },
      less: {
        lessOptions: {
          // If you are using less-loader@5 please spread the lessOptions to options directly
          modifyVars: {  },
          javascriptEnabled: true,
        },
      },
    },
  },
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
      new SitemapPlugin.default(config)
    ]
  //  plugins: [ new SitemapPlugin(process.env.VUE_APP_CLIENT_URL, sitemapJSON.routes, { filename: 'sitemap.xml' }) ]
  },
  transpileDependencies: [
    'vuetify',
  ],
  // chainWebpack: config => {
  // const oneOfsMap = config.module.rule('scss').oneOfs.store
  // oneOfsMap.forEach(item => {
  //   item
  //     .use(['css-loader', 'sass-loader', 'sass-resources-loader'])
  //     .loader('sass-resources-loader')
  //     .options({
  //       // Provide path to the file with resources
  //       resources: [
  //         './src/assets/styles/index.scss',
  //       ],
  //     })
  //     .end()
  // })
  // config.plugins.delete('preload')
  // config.plugins.delete('prefetch')
  // },
}
