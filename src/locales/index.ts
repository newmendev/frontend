import en from './en'
import ru from './ru'

const messages = {
  ru: { ...ru },
  en: { ...en },
}

export default messages
