export interface User {
  photo: string
  name: string
  position: string
  details: string
}
export interface BaseInfo {
  title: string
  address: string
  youtube: string
  instagram: string
  about: string
  teams: User[]
  board: User[]
}

export interface ProjectTypeResult {
  id: string
  type: string
}

export interface ProjectTypes {
  count: number
  next: string
  previous: string
  results: ProjectTypeResult[]
}

export interface Project {
  id: string
  'project_type': string
  title: string
  text: string
  'pic_url': string
  'big_block': boolean
  content: string
}

export interface Projects {
  count: number
  next: string
  previous: string
  results: Project[]
}

export interface Video {
  id: string
  'project_type': string
  title: string
  url: string
  'preview_url': string
  date: string
  'big_block': boolean
}

export interface Videos {
  count: number
  next: string
  previous: string
  results: Video[]
}

export interface OneNews {
  id: string
  title: string
  text: string
  'pic_url': string
  'partner_url': string
  date: string
}

export interface News {
  count: number
  next: string
  previous: string
  results: OneNews[]
}

export interface Post {
  id: string
  title: string
  text: string
  'pic_url': string
  'partner_url': string
  date: string
}

export interface AllPosts {
  count: number
  next: string
  previous: string
  elemID: string
  results: Post[]
}

export interface Category {
  category: []
}

export interface PostTypeResult {
  id: string
  type: string
}

export interface PostTypes {
  count: number
  next: string
  previous: string
  results: PostTypeResult[]
}


