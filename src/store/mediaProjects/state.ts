import { Videos } from '@/services/interfaces/interfaces';

export default {
  videos: [] as Videos|unknown,
  mainVideos: [] as Videos|unknown,
  videoTypes: [] as any,
};
