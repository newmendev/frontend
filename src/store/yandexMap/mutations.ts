import {AllPosts, Category, Post} from '@/services/interfaces/interfaces';

export default {
  getPostsMap: (state: any, value: AllPosts) => {
    state.posts.count = value.count
    state.posts.next = value.next
    state.posts.previous = value.previous
    state.posts.results = state.posts.results.concat(value.results)
  },
  setPostMap: (state: any, value: Post[]) => {
    state.mainPosts = value
  },
};
