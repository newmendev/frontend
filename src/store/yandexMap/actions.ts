import api from '@/services/api/api';
import { MetaTitleDescriptionKeywordsChange } from "@/helpers/commonHelpers";

export default {
  async getPostsMap({ commit, rootState }: any, payload: any) {
    try {
      const params: any = { lang: rootState.lang }
      if (payload.page) params.page = payload.page
      if (payload.pageSize) params.page_size = payload.pageSize
      if (payload.tag) params.tag = payload.tag

      const res = await api.getPosts(params)
      if (payload.pageSize === 6) {
        commit('getPostsMap', res.data.results)
      } else {
        commit('setPostMap', res.data)
      }
      return res
    } catch (error) {
      return error
    }
  },

};
