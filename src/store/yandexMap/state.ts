import {AllPosts, Post} from '@/services/interfaces/interfaces';

export default {
  posts: {
    count: 0,
    next: '',
    previous: '',
    results:[],
  } as AllPosts|unknown,
  upgrades: [] as AllPosts|unknown,
  mainPosts: [] as Post[]|unknown,
  tag: '' as AllPosts | unknown,
};
