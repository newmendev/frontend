const axios = require("axios");
const path = require("path")
const PrerenderSPAPlugin = require('prerender-spa-plugin')
const fs = require("fs");
const Renderer = PrerenderSPAPlugin.PuppeteerRenderer

async function dynamicRoutes() {
  try {
    const env = fs.readFileSync(".env", 'utf-8')
    const arr = env.split('\n')
    let apiUrl = ""
    for (const string of arr) {
      if (string.startsWith("VUE_APP_BASE_API_URL")) {
        apiUrl = string.substring(string.indexOf('=') + 1)
        break
      }
    }
    const { data } = await axios.get(`${apiUrl}v1/seo_data`)
    let projects = data.projects
    let news = data.news
    let projectsPath = projects.map(project => `/projects/project/${project}`)
    let newsPath = news.map(item => `/one-news/${item}`)
    return projectsPath.concat(newsPath)
  }
  catch (e) {
    console.log(e)
  }
}

module.exports = (api, options) => {
  api.registerCommand('build:prerender', async (args) => {
   const dynamicPath = await dynamicRoutes()
    api.chainWebpack(config => {
      config.plugin('prerender').use(PrerenderSPAPlugin, [{
        // Required - The path to the webpack-outputted app to prerender.
        staticDir: path.join(__dirname, 'dist'),
        // Required - Routes to render.
        routes: [ '/' , '/main', '/projects', '/media_projects', '/about_fund', '/news', '/contacts'].concat(dynamicPath),
        renderer: new Renderer({
          renderAfterTime: 5000,
          maxConcurrentRoutes: 4,
        })
        }])
    })
    await api.service.run('build', args)
  })
}

module.exports.defaultModes = {
  'build:prerender': 'production'
}
