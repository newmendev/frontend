const axios = require('axios');
const fs = require('fs')
const staticRoutes = [
  "/projects",
  "/post",
  "/main",
  "/media_projects",
  "/about_fund",
  "/news",
  "/contacts",
  "/personal"
]


async function dynamicRoutes() {
  try {
    const env = fs.readFileSync(".env", 'utf-8')
    const arr = env.split('\n')
    let apiUrl = ""
    for (const string of arr) {
      if (string.startsWith("VUE_APP_BASE_API_URL")) {
        apiUrl = string.substring(string.indexOf('=') + 1)
        break
      }
    }
    const { data } = await axios.get(`${apiUrl}v1/seo_data`)
    let projects = data.projects
    let news = data.news
    let projectsPath = projects.map(project => `/projects/project/${project}`)
    let newsPath = news.map(item => `/one-news/${item}`)
    return projectsPath.concat(newsPath)
  }
  catch (e) {
    console.log(e)
  }
}

async function generateSitemap () {
  const dynamicRoute =  await dynamicRoutes()
  writeSlugsToSitemap(dynamicRoute)
}

function  writeSlugsToSitemap (routes) {
  try {
    fs.writeFileSync('./public/sitemap.json', JSON.stringify({ routes: staticRoutes.concat(routes) }), 'utf-8') }
  catch (err) {
    console.log(err)
  }
}


generateSitemap()
